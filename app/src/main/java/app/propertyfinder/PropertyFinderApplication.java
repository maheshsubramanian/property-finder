package app.propertyfinder;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import roboguice.RoboGuice;

/**
 * @author mahsubramanian
 */
public class PropertyFinderApplication extends Application {

    private static PropertyFinderApplication instance;

    private RequestQueue requestQueue;

    public PropertyFinderApplication() {
        instance = this;
    }

    public static PropertyFinderApplication getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

        requestQueue = Volley.newRequestQueue(this);
    }

}
