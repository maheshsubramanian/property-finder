package app.propertyfinder.model;

/**
 * @author mahsubramanian
 */
public class SortSpinnerOption {

    public static final String PRICE_ASCENDING = "Price (asc)";
    public static final String PRICE_DESCENDING = "Price (desc)";
    public static final String BED_ASCENDING = "Bedrooms (asc)";
    public static final String BED_DESCENDING = "Bedrooms (desc)";

    private String key;
    private String value;

    public SortSpinnerOption(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SortSpinnerOption option = (SortSpinnerOption) o;

        if (!key.equals(option.key)) return false;
        return value.equals(option.value);

    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
