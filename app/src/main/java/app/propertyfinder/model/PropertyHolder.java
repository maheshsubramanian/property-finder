package app.propertyfinder.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author mahsubramanian
 */
public class PropertyHolder {

    @SerializedName("res")
    private List<Property> properties;

    private String total;

    public List<Property> getProperties() {
        return properties;
    }

    public String getTotal() {
        return total;
    }
}
