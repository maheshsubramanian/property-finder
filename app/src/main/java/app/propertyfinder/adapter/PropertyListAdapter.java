package app.propertyfinder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.propertyfinder.R;
import app.propertyfinder.model.Property;
import app.propertyfinder.ui.viewholders.PropertyRowViewHolder;

/**
 * @author mahsubramanian
 */
public class PropertyListAdapter extends ArrayAdapter<Property> {

    private int thumbnailSize;

    public PropertyListAdapter(Context context, int resource, List<Property> objects) {
        super(context, resource, objects);
        thumbnailSize = (int) getContext().getResources().getDimension(R.dimen.thumbnail_size);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Property item = getItem(position);
        final PropertyRowViewHolder viewHolder;
        final View rowView;

        if (null != convertView) {
            viewHolder = (PropertyRowViewHolder) convertView.getTag();
            rowView = convertView;
        } else {
            final LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            rowView = layoutInflater.inflate(R.layout.property_list_item, parent, false);
            viewHolder = new PropertyRowViewHolder();

            viewHolder.setPrice((TextView) rowView.findViewById(R.id.property_list_txtPrice));
            viewHolder.setTitle((TextView) rowView.findViewById(R.id.property_list_txtTitle));
            viewHolder.setImageCount((TextView) rowView.findViewById(R.id.property_list_txtImageCount));
            viewHolder.setLocation((TextView) rowView.findViewById(R.id.property_list_txtLocation));
            viewHolder.setThumbnail((ImageView) rowView.findViewById(R.id.property_list_imgThumbnail));
            rowView.setTag(viewHolder);
        }

        viewHolder.getPrice().setText(item.getPrice() + " " + item.getCurrency());
        viewHolder.getTitle().setText(item.getTitle());
        viewHolder.getImageCount().setText(item.getImageCount());
        viewHolder.getLocation().setText(item.getLocation());

        if (null != item.getThumbnail() && ! item.getThumbnail().trim().isEmpty())
            Picasso.with(getContext()) //
                .load(item.getThumbnail()) //
                .resize(thumbnailSize, thumbnailSize) //
                .into(viewHolder.getThumbnail());

        // Set the data

        return rowView;

    }
}
