package app.propertyfinder.service;

import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import app.propertyfinder.model.Property;
import app.propertyfinder.model.PropertyHolder;
import app.propertyfinder.util.AppUtils;

/**
 * 
 * Mock implementation to return property details whilst offline
 * 
 * @author mahsubramanian
 */
public class OfflineOverrideFetchPropertyServiceImpl extends OnlineFetchPropertyServiceImpl {

    @Inject
    private Context context;

    @Inject
    private Gson gson;

    /**
     * Property to indicate whether to default to online service (false) or mocked offline service (true)
     */
    private boolean override = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Property> retrieveProperty(int pageCount, String sortKey) {

        if (!override) {
            return super.retrieveProperty(pageCount, sortKey);
        }

        if (pageCount > 1) {
            return new ArrayList<>();
        }

        final String booksJSON = AppUtils.loadJSONFromAsset(context, "properties.json");

        final PropertyHolder bookHolder = gson.fromJson(booksJSON, PropertyHolder.class);

        // set the title of the first propertu to be the sort key being passed in
        bookHolder.getProperties().get(0).setTitle(sortKey);

        return bookHolder.getProperties();
    }

    public void setOverride(boolean override) {
        this.override = override;
    }
}
