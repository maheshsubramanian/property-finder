package app.propertyfinder.service;

import android.content.Context;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import app.propertyfinder.PropertyFinderApplication;
import app.propertyfinder.R;
import app.propertyfinder.model.Property;
import app.propertyfinder.model.PropertyHolder;
import roboguice.inject.InjectResource;

/**
 * Real REST-ful implementation to fetch property detail
 * 
 * @author mahsubramanian
 */
public class OnlineFetchPropertyServiceImpl implements FetchPropertyService {

    private static final Logger LOGGER = Logger.getLogger(OnlineFetchPropertyServiceImpl.class.getSimpleName());

    @Inject
    private Context context;

    @Inject
    private Gson gson;

    @InjectResource(R.string.rest_service_url)
    private String restServiceUrl;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Property> retrieveProperty(int pageCount, String sortKey) {
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        String updatedUrl = String.format(restServiceUrl, pageCount, sortKey);
        JsonObjectRequest request = new JsonObjectRequest(updatedUrl, null, future, future);

        PropertyFinderApplication.getInstance().getRequestQueue().add(request);

        try {
            LOGGER.log(Level.INFO, String.format("Making a request to '%s' to fetch properties", updatedUrl));

            // This is a blocking call and will time out in 10 seconds if no response is returned
            JSONObject response = future.get(10, TimeUnit.SECONDS);

            final PropertyHolder holder = gson.fromJson(response.toString(), PropertyHolder.class);

            if (null != holder && holder.getProperties().size() > 0) {
                LOGGER.log(Level.INFO, String.format("Received %d properties", holder.getProperties().size()));
                return holder.getProperties();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            LOGGER.log(Level.SEVERE, "Unable to retrieve property information", e);
        }

        return new ArrayList<>();
    }
}
