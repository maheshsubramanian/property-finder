package app.propertyfinder.service;

import java.util.List;

import app.propertyfinder.model.Property;

/**
 * @author mahsubramanian
 */
public interface FetchPropertyService {

    /**
     * Get the next set of properties corresponding to the given page count
     * 
     * @param pageCount
     * @param sortKey
     * @return
     */
    List<Property> retrieveProperty(int pageCount, String sortKey);

}
