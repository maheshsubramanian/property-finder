package app.propertyfinder.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.propertyfinder.R;
import app.propertyfinder.adapter.PropertyListAdapter;
import app.propertyfinder.model.Property;
import app.propertyfinder.model.SortSpinnerOption;
import app.propertyfinder.service.FetchPropertyService;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Fragment displaying the list of properties
 */
public class PropertyListFragment extends RoboFragment implements AbsListView.OnScrollListener {

    private static final Logger LOGGER = Logger.getLogger(PropertyListFragment.class.getSimpleName());

    private static final int PAGE_COUNT = 20;

    private static final int FETCH_DATA_THRESHOLD = 3;

    private int spinnerListenerSetCount = 1;
    private int scrollListenerSetCount = 1;

    @InjectView(R.id.property_lstProperties)
    private ListView lstProperties;

    @Inject
    private FetchPropertyService fetchPropertyService;

    @InjectView(R.id.progressbar)
    private ProgressBar progressBar;

    @InjectView(R.id.property_spnSort)
    private Spinner spnSort;

    private PropertyListAdapter adapter;

    private volatile boolean dataLoading = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new PropertyListAdapter(getActivity(), R.layout.property_list_item, new ArrayList<Property>());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_property_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ArrayAdapter<SortSpinnerOption> spinnerArrayAdapter =
            new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, getSortOptions());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnSort.setAdapter(spinnerArrayAdapter);

        spnSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SortSpinnerOption option = (SortSpinnerOption) parent.getAdapter().getItem(position);
                if (spinnerListenerSetCount != 1) {
                    resetPropertyAdapter();
                    fetchProperties(1, option.getKey());
                }
                spinnerListenerSetCount++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lstProperties.setAdapter(adapter);
        lstProperties.setOnScrollListener(this);

    }

    private void fetchProperties(final int pageCount, final String sortKey) {
        new AsyncTask<Void, Void, List<Property>>() {

            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected List<Property> doInBackground(Void... params) {
                return fetchPropertyService.retrieveProperty(pageCount, sortKey);
            }

            @Override
            protected void onPostExecute(List<Property> properties) {
                if (!isAdded() && null != properties && properties.size() > 0) {
                    return;
                }

                adapter.addAll(properties);
                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                dataLoading = false;
            }
        }.execute();

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (dataLoading) {
            return;
        }

        if (scrollListenerSetCount == 1) {
            scrollListenerSetCount++;
            return;
        }

        final int adapterCount = adapter.getCount();
        if (firstVisibleItem + visibleItemCount > adapterCount - FETCH_DATA_THRESHOLD) {
            int pageToRetrieve = (adapterCount < PAGE_COUNT) ? 1 : adapterCount / PAGE_COUNT + 1;
            LOGGER.log(Level.FINE, "First visible item is " + firstVisibleItem + " abd adapter count is "
                + adapterCount);
            dataLoading = true;
            fetchProperties(pageToRetrieve, getSortKey());
            LOGGER.log(Level.INFO, "Fetching properties for page count " + pageToRetrieve);
        }
    }

    private void resetPropertyAdapter() {
        adapter.clear();
        adapter.notifyDataSetChanged();
    }

    private List<SortSpinnerOption> getSortOptions() {
        List<SortSpinnerOption> options = new ArrayList<>();
        options.add(new SortSpinnerOption("pa", SortSpinnerOption.PRICE_ASCENDING));
        options.add(new SortSpinnerOption("pd", SortSpinnerOption.PRICE_DESCENDING));
        options.add(new SortSpinnerOption("ba", SortSpinnerOption.BED_ASCENDING));
        options.add(new SortSpinnerOption("bd", SortSpinnerOption.BED_DESCENDING));
        return options;
    }

    private String getSortKey() {
        return ((SortSpinnerOption) spnSort.getSelectedItem()).getKey();
    }

}
