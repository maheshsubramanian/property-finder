package app.propertyfinder.ui.viewholders;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author mahsubramanian
 */
public class PropertyRowViewHolder {

    private ImageView thumbnail;

    private TextView imageCount;

    private TextView price;

    private TextView location;

    private TextView title;

    public ImageView getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public TextView getImageCount() {
        return imageCount;
    }

    public void setImageCount(TextView imageCount) {
        this.imageCount = imageCount;
    }

    public TextView getPrice() {
        return price;
    }

    public void setPrice(TextView price) {
        this.price = price;
    }

    public TextView getLocation() {
        return location;
    }

    public void setLocation(TextView location) {
        this.location = location;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }
}
