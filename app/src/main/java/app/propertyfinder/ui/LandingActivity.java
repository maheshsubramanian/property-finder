package app.propertyfinder.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import app.propertyfinder.R;
import app.propertyfinder.ui.fragment.PropertyListFragment;
import roboguice.activity.RoboActionBarActivity;
import roboguice.activity.RoboFragmentActivity;

/**
 * Activity displaying the fragment for listing properties
 */
public class LandingActivity extends RoboActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        Fragment propertyListFragment = new PropertyListFragment();
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.property_listContainer, propertyListFragment, PropertyListFragment.class.getCanonicalName())
            .commit();
    }

}
