package app.propertyfinder;

import com.google.inject.Binder;
import com.google.inject.Module;

import javax.inject.Singleton;

import app.propertyfinder.service.FetchPropertyService;
import app.propertyfinder.service.OfflineOverrideFetchPropertyServiceImpl;

/**
 * Module class defining all the application wide bindings
 * 
 * @author mahsubramanian
 */
public class DependencyModule implements Module {

    @Override
    public void configure(Binder binder) {

        binder.bind(FetchPropertyService.class).to(OfflineOverrideFetchPropertyServiceImpl.class).in(Singleton.class);

    }
}
