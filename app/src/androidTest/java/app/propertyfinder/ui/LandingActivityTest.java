package app.propertyfinder.ui;

import android.widget.ListView;

import org.hamcrest.CoreMatchers;

import app.propertyfinder.R;
import app.propertyfinder.model.Property;
import app.propertyfinder.model.SortSpinnerOption;
import app.propertyfinder.support.AbstractEspressoActivityTest;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.instanceOf;

/**
 * UI test for landing activity
 * 
 * @author mahsubramanian
 */
public class LandingActivityTest extends AbstractEspressoActivityTest<LandingActivity> {

    private static final int EXPECTED_COUNT_OF_LIST_ITEMS = 20;
    private LandingActivity activity;

    public LandingActivityTest() {
        super(LandingActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();

    }

    /**
     * Test to check the activity is displayed with all UI elements visible
     */
    public void test_LandingActivityLoadedWithAllViewCorrectlyDisplayed() {

        assertNotNull("Activity should not be null", activity);

        // list view is displayed
        onView(withId(R.id.property_lstProperties)).check(matches(isDisplayed()));

        // sort option selection spinner displayed
        onView(withId(R.id.property_spnSort)).check(matches(isDisplayed()));

    }

    /**
     * Test to check the property list is loaded and it contains the right number of entries (20 in this case)
     */
    public void test_ListViewDisplayedWithCorrectEntries() {

        assertNotNull("Activity should not be null", activity);

        // list view is displayed
        onView(withId(R.id.property_lstProperties)).check(matches(isDisplayed()));

        ListView listProperties = (ListView) activity.findViewById(R.id.property_lstProperties);
        assertEquals("List view should have 20 items in it", EXPECTED_COUNT_OF_LIST_ITEMS, listProperties.getAdapter()
            .getCount());

    }

    /**
     * Test to check the sort option spinner entries are correctly passed to the service layer
     */
    public void test_ListViewUpdatedCorrectlyWhenSortOptionSelected() throws InterruptedException {

        assertNotNull("Activity should not be null", activity);

        // list view is displayed
        onView(withId(R.id.property_lstProperties)).check(matches(isDisplayed()));

        ListView listProperties = (ListView) activity.findViewById(R.id.property_lstProperties);

        onView(withId(R.id.property_spnSort)).check(matches(withSpinnerText(SortSpinnerOption.PRICE_ASCENDING)));

        // select option to sort properties by price in descending order
        onView(withId(R.id.property_spnSort)).perform(click());
        onData(
            CoreMatchers.allOf(is(instanceOf(SortSpinnerOption.class)), is(new SortSpinnerOption("pd",
                SortSpinnerOption.PRICE_DESCENDING)))).perform(click());

        waitForAllPendingAsyncTasks(activity);

        // verify title of 1st property matches sort selection key
        onData(instanceOf(Property.class)).inAdapterView(withId(R.id.property_lstProperties)).atPosition(0)
            .onChildView(withId(R.id.property_list_txtTitle)).check(matches(withText("pd")));

        // select option to sort properties by bed in ascending order
        onView(withId(R.id.property_spnSort)).perform(click());
        onData(
                CoreMatchers.allOf(is(instanceOf(SortSpinnerOption.class)), is(new SortSpinnerOption("ba",
                        SortSpinnerOption.BED_ASCENDING)))).perform(click());

        waitForAllPendingAsyncTasks(activity);

        // verify title of 1st property matches sort selection key
        onData(instanceOf(Property.class)).inAdapterView(withId(R.id.property_lstProperties)).atPosition(0)
                .onChildView(withId(R.id.property_list_txtTitle)).check(matches(withText("ba")));

        // select option to sort properties by bed in descending order
        onView(withId(R.id.property_spnSort)).perform(click());
        onData(
                CoreMatchers.allOf(is(instanceOf(SortSpinnerOption.class)), is(new SortSpinnerOption("bd",
                        SortSpinnerOption.BED_DESCENDING)))).perform(click());

        waitForAllPendingAsyncTasks(activity);

        // verify title of 1st property matches sort selection key
        onData(instanceOf(Property.class)).inAdapterView(withId(R.id.property_lstProperties)).atPosition(0)
                .onChildView(withId(R.id.property_list_txtTitle)).check(matches(withText("bd")));

    }

}