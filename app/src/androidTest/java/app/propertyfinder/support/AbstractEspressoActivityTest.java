package app.propertyfinder.support;

import android.app.Activity;
import android.os.AsyncTask;
import android.test.ActivityInstrumentationTestCase2;

import com.google.inject.Injector;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import app.propertyfinder.service.FetchPropertyService;
import app.propertyfinder.service.OfflineOverrideFetchPropertyServiceImpl;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * Base class with common functionality for Espresso tests.
 *
 * @author mahsubramanian
 */
public abstract class AbstractEspressoActivityTest<T extends Activity> extends ActivityInstrumentationTestCase2<T> {

    private static final long WAIT_FOR_THREAD_TIMEOUT = 10000;

    private final Class<T> activityClass;

    public AbstractEspressoActivityTest(Class<T> activityClass) {
        super(activityClass);
        this.activityClass = activityClass;
    }

    /**
     * Set up
     *
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    /**
     * @param activity
     * @return
     */
    protected RoboInjector getInjector(Activity activity) {
        return RoboGuice.getInjector(activity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getActivity() {

        final T activity = super.getActivity();

        final OfflineOverrideFetchPropertyServiceImpl fetchPropertyService =
            (OfflineOverrideFetchPropertyServiceImpl) getInjector(activity).getInstance(FetchPropertyService.class);
        fetchPropertyService.setOverride(true);

        return activity;
    }

    /**
     * Subclasses do not have to invoke this method.
     *
     * @param injector
     */
    protected void clearDb(Injector injector) {
        // do nothing
    }

    /**
     * Wait for all {@link android.os.AsyncTask}s <strong>before this method was invoked</strong> to complete their
     * execution.
     *
     * @param activity
     * @throws InterruptedException
     */
    protected void waitForAllPendingAsyncTasks(Activity activity) throws InterruptedException {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        countDownLatch.countDown();
                        return null;
                    }
                }.execute();
            }
        });
        countDownLatch.await(WAIT_FOR_THREAD_TIMEOUT, TimeUnit.MILLISECONDS);
    }

}
